/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.travelsis.controller;

import br.com.travelsis.usuario.Usuario;
import br.com.travelsis.usuario.UsuarioRN;
import br.com.travelsis.usuario.UsuarioTableModel;
import br.com.travelsis.view.CadastroUsuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author LAB-1206
 */
public class UsuarioController implements ActionListener, MouseListener {

    private CadastroUsuario cadastroUsuario;
    private List<Usuario> usuarioList;
    private Usuario usuarioSelected;

    public UsuarioController(CadastroUsuario cadastroUsuario) {
        UsuarioRN usuarioRN = new UsuarioRN();
        this.cadastroUsuario = cadastroUsuario;
        this.usuarioList = usuarioRN.list();
        this.preencherTabela();
    }

    public void preencherTabela() {
        UsuarioRN usuarioRN = new UsuarioRN();
        this.usuarioList = usuarioRN.list();
        UsuarioTableModel usuarioTableModel = new UsuarioTableModel(usuarioList);
        cadastroUsuario.getTbUsuarios().setModel(usuarioTableModel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cadastroUsuario.getBtSalvar()) {
            //Criar o obj 
            Usuario usuario = new Usuario();
            //Passar valor com base nos campos de texto
            usuario.setLogin(cadastroUsuario.getTfUsuario().getText());
            usuario.setSenha(cadastroUsuario.getTfSenha().getText());
            usuario.setNome(cadastroUsuario.getTfNome().getText());
            usuario.setConfirmacao(cadastroUsuario.getTfConfirmaSenha().getText());
            //Criar a Regra de Negócio
            UsuarioRN urn = new UsuarioRN();
            JOptionPane.showMessageDialog(null, urn.save(usuario));
            this.preencherTabela();

        } else if (e.getSource() == cadastroUsuario.getBtLimpar()) {
            limpar();
        } else if (e.getSource() == cadastroUsuario.getBtDeletar()) {
            UsuarioRN urn = new UsuarioRN();
            JOptionPane.showMessageDialog(null, urn.delete(usuarioSelected));
            preencherTabela();
            limpar();

        }

    }

    public void limpar() {
        cadastroUsuario.getTfNome().setText("");
        cadastroUsuario.getTfSenha().setText("");
        cadastroUsuario.getTfConfirmaSenha().setText("");
        cadastroUsuario.getTfUsuario().setText("");
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int row = cadastroUsuario.getTbUsuarios().getSelectedRow();
        this.usuarioSelected = usuarioList.get(row);
        this.cadastroUsuario.getTfNome().setText(usuarioSelected.getNome());
        this.cadastroUsuario.getTfUsuario().setText(usuarioSelected.getLogin());
        this.cadastroUsuario.getTfSenha().setText("");
        this.cadastroUsuario.getTfConfirmaSenha().setText("");
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
