/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.travelsis.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Conexao {
    private static final String USER = "root";
    private static final String PASSWORD = "";
    private static final String URL = "jdbc:mysql://localhost/jdbcbank";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static Connection connection = null;
    
    public static Connection getConnection(){
        try{
            //Registro do Driver de conexão
            Class.forName(DRIVER);
            // Atribuir a conexão à variável
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Conectado com sucesso!");
        }catch(ClassNotFoundException | SQLException ex){
            System.out.println("Erro ao conectar!");
            ex.printStackTrace();
        }
        return connection;
    }
}
