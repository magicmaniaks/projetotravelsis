/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.travelsis.usuario;

import br.com.travelsis.util.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LAB-1206
 */
public class UsuarioDAO {

    public UsuarioDAO() {
        this.connection = Conexao.getConnection();
    }
    private Connection connection;
    
    public List<Usuario> getAllUsuarios(){
        List<Usuario> usuarios = new ArrayList<>();
        try{
            String sql = "SELECT * FROM usuario";
            //Passa os parâmetros
            PreparedStatement stmt = connection.prepareStatement(sql);
            //Pega os resultados
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                //construir o usuário
                Usuario usuario  = new Usuario();
                usuario.setId(rs.getInt("id"));
                usuario.setNome(rs.getString("nome"));
                usuario.setLogin(rs.getString("login"));
                usuario.setSenha(rs.getString("senha"));
                //adicionar ele na lista
                usuarios.add(usuario);
            }
            stmt.close();
            rs.close();
            connection.close();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return usuarios;
    }

    public void insertUsuario(Usuario usuario) {
        try{
        //COMANDOS SQL    
        String sql = "INSERT INTO Usuario (nome,login,senha) VALUES (?,?,?)";
        //PRECOMPILADO QUE IRÁ FALAR COM O BD
        PreparedStatement stmt = connection.prepareStatement(sql);
        //PARÂMETROS
        stmt.setString(1, usuario.getNome());
        stmt.setString(2, usuario.getLogin());
        stmt.setString(3, usuario.getSenha());
        stmt.execute();
        stmt.close();
        connection.close();
        }catch(SQLException ex){
            System.out.println("Algo deu errado!" + ex.getMessage());
        }
        
    }
     public void updateUsuario(Usuario usuario) {
        try{
        //COMANDOS SQL    
        String sql = "UPDATE Usuario SET nome = ?, login =? ,senha =?"
                + "WHERE id = ?";
        //PRECOMPILADO QUE IRÁ FALAR COM O BD
        PreparedStatement stmt = connection.prepareStatement(sql);
        //PARÂMETROS
        stmt.setString(1, usuario.getNome());
        stmt.setString(2, usuario.getLogin());
        stmt.setString(3, usuario.getSenha());
        stmt.setInt(4, usuario.getId());
        stmt.execute();
        stmt.close();
        connection.close();
        }catch(SQLException ex){
            System.out.println("Algo deu errado!" + ex.getMessage());
        }
        
    }
     public void deleteUsuario(Usuario usuario) {
        try{
        //COMANDOS SQL    
        String sql = "DELETE FROM Usuario WHERE id = ?";
        //PRECOMPILADO QUE IRÁ FALAR COM O BD
        PreparedStatement stmt = connection.prepareStatement(sql);
        //PARÂMETROS
        stmt.setInt(1, usuario.getId());
        stmt.execute();
        stmt.close();
        connection.close();
        }catch(SQLException ex){
            System.out.println("Algo deu errado!" + ex.getMessage());
        }
        
    }
    
}
