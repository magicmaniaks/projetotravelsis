/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.travelsis.usuario;

import java.util.List;

/**
 *
 * @author LAB-1206
 */
public class UsuarioRN {

    public String save(Usuario usuario) {
        if (usuario.getSenha().equals(usuario.getConfirmacao())) {
            if (usuario.getSenha().length() < 8) {
                return "A senha deverá ser maior que 8 caracteres";
            } else {
                String loginAux = usuario.getLogin().replaceAll(" ", "");
                usuario.setLogin(loginAux);
                UsuarioDAO udao = new UsuarioDAO();
                udao.insertUsuario(usuario);
                return "Dados inseridos com sucesso!";
            }
        } else {
            return "Senha e confirmação devem ser iguais!";
        }
    }

    public String delete(Usuario usuario) {
        UsuarioDAO udao = new UsuarioDAO();
        udao.deleteUsuario(usuario);
        return "Dados deletados com sucesso!";
    }

    public List<Usuario> list() {
        UsuarioDAO udao = new UsuarioDAO();
        return udao.getAllUsuarios();
    }
}
