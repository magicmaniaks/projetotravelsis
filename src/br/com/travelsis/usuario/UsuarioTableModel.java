/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.travelsis.usuario;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author LAB-901
 */
public class UsuarioTableModel extends AbstractTableModel{
    private List<Usuario> usuarios;
    private static final int ID = 0;
    private static final int NOME = 1;
    private static final int LOGIN = 2;

    public UsuarioTableModel(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
    
    
    @Override
    public int getRowCount() {
       return usuarios.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Usuario usuario = usuarios.get(rowIndex);
        if(columnIndex == ID){
            return usuario.getId();
        }
        if(columnIndex == LOGIN){
            return usuario.getLogin();
        }
        if(columnIndex == NOME){
            return  usuario.getNome();
        }
        return null;
    }

    @Override
    public String getColumnName(int columnIndex) {
         if(columnIndex == ID){
            return "Id";
        }
        if(columnIndex == LOGIN){
            return "Login";
        }
        if(columnIndex == NOME){
            return  "Nome";
        }
        return null;
    }
    
}
